import './HorizontalAccordion.css';
import { useState } from "react";
import OutlinedButton from "../OutlinedButton/OutlinedButton";

function HorizontalAccordion({ events }) {

    const [activeItemId, setActiveItemId] = useState(events[0].id);

    function normalizeId(id) {
        return String(id).padStart(2, '0');
    }

    function normalizeDate(date) {
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const year = date.getFullYear();
        return `${day}-${month}-${year}`;
    }

    function showItem(id) {
        setActiveItemId(id);
    }

    return (
        <div className="gallery-wrapper">
            {events.map((event, index) => (
                <div className={`gallery-item ${event.id === activeItemId ? 'active' : ''}`}
                     onClick={() => showItem(event.id)}
                     key={event.id}
                >
                    <div className="gallery-header-wrapper"
                         style={{ backgroundImage: `url(${event.img})` }}
                    >
                        <div className="gallery-header">
                            <span className="gallery-header-id">{normalizeId(index + 1)}</span>
                            <span className="gallery-header-label">{event.label}</span>
                        </div>
                    </div>
                    <div className="gallery-body-wrapper" style={{ backgroundImage: `url(${event.img})` }}>
                        <div className="gallery-body">
                            <div className="gallery-body-info">
                                <div className="gallery-body-info-id">{normalizeId(index + 1)}</div>
                                <span className="gallery-body-info-title">{event.label}</span>
                                <span className="gallery-body-info-date">{normalizeDate(new Date())}</span>
                                <OutlinedButton text="More information" style={{ marginTop: 15 }}/>
                            </div>
                            <div className="gallery-body-stub">&nbsp;</div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default HorizontalAccordion;
