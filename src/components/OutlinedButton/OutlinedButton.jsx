import './OutlinedButton.css'

function OutlinedButton({ text, style, onClick }) {
    return (
        <div className="more-info-button-wrapper" style={style} onClick={onClick}>
            <span>{text}</span>
        </div>
    );
}

export default OutlinedButton
