import './App.css';
import HorizontalAccordion from "./components/HorizontalAccordion/HorizontalAccordion";
import SOCIAL_EVENTS from './socialEvents.json';

function App() {
    return (
        <div className="App">
            <div className="container">
                <h1>Gallery</h1>
                <HorizontalAccordion events={SOCIAL_EVENTS}/>
            </div>
        </div>
    );
}

export default App;
